package com.example.tp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsyncTaskImage extends AsyncTask<String, String, Bitmap> {

    private URL ImageUrl = null;
    private InputStream is = null;
    private Bitmap bmImg = null;
    private ImageView imageView;
    private Team team ;

    public AsyncTaskImage(ImageView i ,Team t){
        imageView = i ;
        team = t ;
    }
    @Override
    protected Bitmap doInBackground(String... strings) {
        try {
            ImageUrl = new URL(team.getTeamBadge());
            HttpURLConnection conn = (HttpURLConnection) ImageUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            bmImg = BitmapFactory.decodeStream(is, null, options);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmImg;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageView.setImageBitmap(bitmap);
    }


}
