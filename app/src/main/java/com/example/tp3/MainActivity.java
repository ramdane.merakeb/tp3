package com.example.tp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import com.example.tp3.Team;
import com.example.tp3.TeamActivity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class MainActivity extends AppCompatActivity {

    private SportDbHelper databaseTeam ;
    private ListView listView;
    private Cursor cursor;
    public static SimpleCursorAdapter adapter;
    public Team team ;
    static final int REQUEST_CODE_NEW_TEAM = 1;
    static final int REQUEST_CODE_TEAM = 2;
    public static SwipeRefreshLayout mSwipeRefreshLayout;
    public static int nbrThreadsRunningRefresh = 0;
   public List<Team> res = new ArrayList<>();






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);

        databaseTeam = new SportDbHelper(this);

        //inserer dans la Base de données
        databaseTeam.populate();

        //recuperer les données de la BD
        cursor = databaseTeam.fetchAllTeams();
        cursor.moveToFirst();

        //on lie la BD avec La listView
        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor,
                new String[]{
                        //Tableau de colonnes  aux quelles on va  lier de curseur
                        SportDbHelper.COLUMN_TEAM_NAME,
                        SportDbHelper.COLUMN_LEAGUE_NAME
                },

                new int[]{
                        //Tableau de colonnes  aux quelles on va  lier de curseur
                        android.R.id.text1, android.R.id.text2
                },
                0);


        listView = (ListView) findViewById(R.id.liste_team);
        listView.setAdapter(adapter);


        // clique sur un item pour lancer la WineActivity
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                Intent intentTeam = new Intent(view.getContext(), TeamActivity.class);
                Cursor cursorTeam = (Cursor) listView.getItemAtPosition(position);
                Team teamSelected = databaseTeam.cursorToTeam(cursorTeam);
                Log.d("Ouverture de teamS", teamSelected.toString());
                intentTeam.putExtra("TeamActivity", teamSelected);
                startActivityForResult(intentTeam , REQUEST_CODE_TEAM);;
            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(),NewTeamActivity.class);
                startActivityForResult(intent , REQUEST_CODE_NEW_TEAM);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e(getClass().getSimpleName(), "refresh");
                for (Team team : databaseTeam.getAllTeams()) {
                    AsyncTaskAllTeam runningTask = new AsyncTaskAllTeam();
                    runningTask.execute(team);

                    MainActivity.nbrThreadsRunningRefresh++;
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            //le cas d'un ajou d'une nouvelle team
            if (requestCode == REQUEST_CODE_NEW_TEAM  && resultCode  == RESULT_OK) {

                // on recupere la Team ajouter recu de NewTeamActivity
               // Bundle extras = getIntent().getExtras();
                //team = (Team) extras.get("NewTeamActivity");
                team = data.getParcelableExtra("NewTeamActivity");
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Ajout d'une nouvelle Team")
                        .setMessage("Nom:  "+team.getName()+"  Ligue:  "+team.getLeague())
                        .show();
                //ajout du team recuperer de NewTeamActivity
                databaseTeam.addTeam(team);
            }
            //le cas de mise a jour d'une team existant recu de TeamActivivity
            if (requestCode == REQUEST_CODE_TEAM  && resultCode  == RESULT_OK) {

                // on recupere la Team a mettre a jour
                team = data.getParcelableExtra("TeamActivity");

                //mis a jour du team recuperer de TeamActivity
                databaseTeam.updateTeam(team);
            }

        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //pour reconstruir les donnees et mettre a jour l'activite  a chaque interventions par cette methode
    @Override
    protected void onResume() {
        super.onResume();
        cursor = databaseTeam.fetchAllTeams();
        adapter.changeCursor(cursor);
        adapter.notifyDataSetChanged();

    }

    public class AsyncTaskAllTeam extends AsyncTask<Team, String, List<Team>> {

        private JSONResponseHandlerRankingTotal JSONRanking ;
        private JSONResponseHandlerLastEvent JSONLastTeam;
        private JSONResponseHandlerTeam JSONTeam;
        private URL urlTeam, urlLastEvent ,urlRanking;
        private InputStream isTeam , isLastEvent , isRanking;
        HttpsURLConnection urlConnectionTeam , urlConnectionLastEvent , urlConnectionRanking = null;

        @Override
        protected List<Team> doInBackground(Team... Teams) {
            Team team = Teams[0];

            try {

                // recuperer les information des team
                urlTeam = WebServiceUrl.buildSearchTeam(team.getName());
                urlConnectionTeam = (HttpsURLConnection) urlTeam.openConnection();

                // recuperer le dernier match
                urlLastEvent = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnectionLastEvent = (HttpsURLConnection) urlLastEvent.openConnection();

                //recuperer le classement et le score totale
                urlRanking = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlConnectionRanking = (HttpsURLConnection) urlRanking.openConnection();


                if (urlConnectionTeam.getResponseCode() == 200 && urlConnectionLastEvent.getResponseCode() == 200 && urlConnectionRanking.getResponseCode() == 200) {

                    // recuperer les information des team
                    isTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                    JSONTeam = new JSONResponseHandlerTeam(team);
                    JSONTeam.readJsonStreamTeam(isTeam);

                    // recuperer le dernier match
                    isLastEvent = new BufferedInputStream(urlConnectionLastEvent.getInputStream());
                    JSONLastTeam = new JSONResponseHandlerLastEvent(team);
                    JSONLastTeam.readJsonStreamLastEvent(isLastEvent);

                    // recuperer le classement et le score total
                    isRanking = new BufferedInputStream(urlConnectionRanking.getInputStream());
                    JSONRanking = new JSONResponseHandlerRankingTotal(team);
                    JSONRanking.readJsonStreamRankingTotal(isRanking);

                    res.add(team);


                } else {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Erreur")
                            .setMessage("erreur internet.")
                            .show();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return res;



        }

        @Override
        protected void onPostExecute(List<Team> allTeams) {
            super.onPostExecute(allTeams);

            MainActivity.nbrThreadsRunningRefresh--;
            if (MainActivity.nbrThreadsRunningRefresh <= 0) {

                for (Team team : allTeams) {
                    databaseTeam.updateTeam(team);
                }


                // Update the listview
                cursor = databaseTeam.fetchAllTeams();
                adapter.changeCursor(cursor);
                adapter.notifyDataSetChanged();

                mSwipeRefreshLayout.setRefreshing(false);

                Log.d("RefreshTeamContent","Finished");
            }



        }
    }

}
