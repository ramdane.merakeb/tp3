
package com.example.tp3;

        import android.util.JsonReader;
        import android.util.Log;

        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.util.Date;

        import com.example.tp3.Team;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=R
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerRankingTotal {

    private static final String TAG = JSONResponseHandlerRankingTotal.class.getSimpleName();

    private Team team;
    private String teamNameCourrant ;
    private String teamName ;


    public JSONResponseHandlerRankingTotal(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStreamRankingTotal(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readRankingTotal(reader);
        } finally {
            reader.close();
        }
    }

    public void readRankingTotal(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayRankingTotal(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayRankingTotal(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 1;
        while (reader.hasNext() ) {
            reader.beginObject();

            while (reader.hasNext()) {
                String name = reader.nextName();
                if ((name != null) && (name.equals("name"))) {
                    teamName = reader.nextString();
                    teamNameCourrant = team.getName();

                    if (teamName != null && teamName.equals(teamNameCourrant)) {
                        team.setRanking(nb);
                    }

                    while (reader.hasNext()) {
                        String nameJSON = reader.nextName();

                        if (nameJSON.equals("total")) {
                            team.setTotalPoints(reader.nextInt());
                            break;
                        } else {
                            reader.skipValue();
                        }
                    }

                } else {

                    if (reader.hasNext()) {
                        reader.skipValue();
                    }
                }
            }


            reader.endObject();
            nb++;
        }
        reader.endArray();
    }


}
