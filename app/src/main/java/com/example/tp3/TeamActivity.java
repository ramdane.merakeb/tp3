package com.example.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import com.example.tp3.Match;
import com.example.tp3.Team;
import com.example.tp3.WebServiceUrl;

import javax.net.ssl.HttpsURLConnection;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("TeamActivity");

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO
                AsyncTaskTeam asyncTask = new AsyncTaskTeam();
                asyncTask.execute(team);

            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        //envoie du team a MainActivity
        Intent intent = new Intent();
        intent.putExtra("TeamActivity", team);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        //TODO : update imageBadge
        AsyncTaskImage asyncTask = new AsyncTaskImage(imageBadge, team);
        asyncTask.execute();


    }



    public class AsyncTaskTeam extends AsyncTask<Team, String, Boolean> {

        private JSONResponseHandlerRankingTotal JSONRanking ;
        private JSONResponseHandlerLastEvent JSONLastTeam;
        private JSONResponseHandlerTeam JSONTeam;
        private URL urlTeam, urlLastEvent ,urlRanking;
        private InputStream isTeam , isLastEvent , isRanking;
        HttpsURLConnection urlConnectionTeam , urlConnectionLastEvent , urlConnectionRanking = null;

        @Override
        protected Boolean doInBackground(Team... Teams) {
                Team t = Teams[0];
            try {

                // recuperer les information des team
                urlTeam = WebServiceUrl.buildSearchTeam(team.getName());
                urlConnectionTeam = (HttpsURLConnection) urlTeam.openConnection();

                // recuperer le dernier match
                urlLastEvent = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnectionLastEvent = (HttpsURLConnection) urlLastEvent.openConnection();

                //recuperer le classement et le score totale
                urlRanking = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlConnectionRanking = (HttpsURLConnection) urlRanking.openConnection();


                if (urlConnectionTeam.getResponseCode() == 200 && urlConnectionLastEvent.getResponseCode() == 200 && urlConnectionRanking.getResponseCode() == 200) {

                    // recuperer les information des team
                    isTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                    JSONTeam = new JSONResponseHandlerTeam(team);
                    JSONTeam.readJsonStreamTeam(isTeam);
                    urlConnectionTeam.disconnect();



                    // recuperer le dernier match
                    isLastEvent = new BufferedInputStream(urlConnectionLastEvent.getInputStream());
                    JSONLastTeam = new JSONResponseHandlerLastEvent(team);

                    JSONLastTeam.readJsonStreamLastEvent(isLastEvent);
                    urlConnectionLastEvent.disconnect();



                    // recuperer le classement et le score total
                    isRanking = new BufferedInputStream(urlConnectionRanking.getInputStream());
                    JSONRanking = new JSONResponseHandlerRankingTotal(team);
                    JSONRanking.readJsonStreamRankingTotal(isRanking);
                    urlConnectionRanking.disconnect();




                } else {
                    new AlertDialog.Builder(TeamActivity.this)
                            .setTitle("Erreur")
                            .setMessage("erreur internet.")
                            .show();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;



        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            updateView();

        }
    }

}
